﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using XGE.Engine;

namespace Amputator.Gui
{
    class GuiButton : XGE.Engine.Gui
    {
        protected String text;
        protected SpriteFont font;

        public event EventHandler OnClick;

        public GuiButton(BaseGame game, String name, String fontname, Rectangle Rectangle)
            : base(game)
        {
            rectangle = Rectangle;
            texture = BaseGame.Content.Load<Texture2D>(name);
            font = BaseGame.Content.Load<SpriteFont>(fontname);
            color = Color.Black;
            text = "button";
        }

        public String Text
        {
            get
            {
                return name;
            }
            set
            {
                text = (String)value;
            }
        }

        public override void Update(GameTime gameTime)
        {
            MouseState state = Mouse.GetState();
            if (
                state.X > rectangle.X &&
                state.Y > rectangle.Y &&
                state.X < rectangle.X + rectangle.Width &&
                state.Y < rectangle.Y + rectangle.Height)
            {
                Color = Color.Red;
                if (OnClick != null && state.LeftButton == ButtonState.Pressed) OnClick(this, null);
            }
            else
            {
                Color = Color.White;
            }
            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            BaseGame.Sprite.Draw(texture, rectangle, color);
            BaseGame.Sprite.DrawString(font, text, new Vector2(rectangle.X + rectangle.Width / 2 - font.MeasureString(text).X / 2, rectangle.Y + rectangle.Height / 2 - font.MeasureString(text).Y / 2), color);
        }
    }
}
