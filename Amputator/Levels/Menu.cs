﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using XGE;
using XGE.GameObjects;
using XGE.Engine;
using JigLibX;
using JigLibX.Collision;
using JigLibX.Physics;

namespace Amputator.Levels
{
    class Menu : Level
    {
        public Gui.GuiButton buttonSaveGame;
        public Gui.GuiButton buttonLoadGame;
        public Gui.GuiButton buttonExitGame;

        public Menu(BaseGame game)
            : base(game)
        {
        }

        public override void Load()
        {
            buttonSaveGame = new Amputator.Gui.GuiButton(BaseGame, "guiback", "guifont", new Rectangle(BaseGame.Graphics.PreferredBackBufferWidth / 2 - 100, BaseGame.Graphics.PreferredBackBufferHeight / 2 - 20 - 55, 200, 40));
            buttonSaveGame.Text = "save game";
            buttonSaveGame.Color = Color.White;
            gui.Add(buttonSaveGame);

            buttonLoadGame = new Amputator.Gui.GuiButton(BaseGame, "guiback", "guifont", new Rectangle(BaseGame.Graphics.PreferredBackBufferWidth / 2 - 100, BaseGame.Graphics.PreferredBackBufferHeight / 2 - 20, 200, 40));
            buttonLoadGame.Text = "load game";
            buttonLoadGame.Color = Color.White;
            gui.Add(buttonLoadGame);

            buttonExitGame = new Amputator.Gui.GuiButton(BaseGame, "guiback", "guifont", new Rectangle(BaseGame.Graphics.PreferredBackBufferWidth / 2 - 100, BaseGame.Graphics.PreferredBackBufferHeight / 2 - 20 + 55, 200, 40));
            buttonExitGame.Text = "exit";
            buttonExitGame.Color = Color.White;
            buttonExitGame.OnClick += new EventHandler(((Game1)BaseGame).Exit);
            gui.Add(buttonExitGame);

            BaseGame.IsMouseVisible = true;
            BaseGame.GameState = GameState.Paused;

            base.Load();
        }
        public override void Unload()
        {
            base.Unload();
            BaseGame.IsMouseVisible = false;
            BaseGame.GameState = GameState.Playing;
        }
    }
}