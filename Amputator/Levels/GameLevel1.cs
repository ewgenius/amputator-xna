﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using XGE;
using XGE.GameObjects;
using XGE.Engine;
using JigLibX;
using JigLibX.Collision;
using JigLibX.Physics;

namespace Amputator.Levels
{
    class GameLevel1 : Level
    {
        Model modelterrain;
        Model modelbox;

        HeightmapObject terrain;

        public GameLevel1(BaseGame game)
            : base(game)
        {
        }
        public override void Load()
        {
            modelterrain = BaseGame.Content.Load<Model>("terrain");
            modelbox = BaseGame.Content.Load<Model>("box");

            terrain = new HeightmapObject(BaseGame, modelterrain, null, Vector2.Zero);
            objects.Add(terrain);

            for (int i = 0; i < 10; i++)
            {
                BoxObject pich = new BoxObject(BaseGame, modelbox, null, Vector3.One, Matrix.Identity, new Vector3(0, 2 * i + 10, 0));
                objects.Add(pich);
            }
            base.Load();
            BaseGame.IsMouseVisible = false;
            BaseGame.GameState = GameState.Playing;
        }
        public override void Unload()
        {
            base.Unload();
            terrain.Dispose();
        }
    }
}
