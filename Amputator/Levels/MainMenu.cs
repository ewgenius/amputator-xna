﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using XGE;
using XGE.GameObjects;
using XGE.Engine;
using JigLibX;
using JigLibX.Collision;
using JigLibX.Physics;

namespace Amputator.Levels
{
    class MainMenu : Level
    {
        public Gui.GuiButton buttonNewGame;
        public Gui.GuiButton buttonLoadGame;
        public Gui.GuiButton buttonSettings;
        public Gui.GuiButton buttonExitGame;

        public MainMenu(BaseGame game)
            : base(game)
        {
        }

        public override void Load()
        {
            buttonNewGame = new Amputator.Gui.GuiButton(BaseGame, "guiback", "guifont", new Rectangle(BaseGame.Graphics.PreferredBackBufferWidth / 2 - 100, BaseGame.Graphics.PreferredBackBufferHeight / 2 - 20 - 75, 200, 40));
            buttonNewGame.Text = "new game";
            buttonNewGame.Color = Color.White;
            buttonNewGame.OnClick += new EventHandler(((Game1)BaseGame).NextLevel);
            gui.Add(buttonNewGame);

            buttonLoadGame = new Amputator.Gui.GuiButton(BaseGame, "guiback", "guifont", new Rectangle(BaseGame.Graphics.PreferredBackBufferWidth / 2 - 100, BaseGame.Graphics.PreferredBackBufferHeight / 2 - 20 - 25, 200, 40));
            buttonLoadGame.Text = "load game";
            buttonLoadGame.Color = Color.White;
            gui.Add(buttonLoadGame);

            buttonSettings = new Amputator.Gui.GuiButton(BaseGame, "guiback", "guifont", new Rectangle(BaseGame.Graphics.PreferredBackBufferWidth / 2 - 100, BaseGame.Graphics.PreferredBackBufferHeight / 2 - 20 + 25, 200, 40));
            buttonSettings.Text = "settings";
            buttonSettings.Color = Color.White;
            gui.Add(buttonSettings);

            buttonExitGame = new Amputator.Gui.GuiButton(BaseGame, "guiback", "guifont", new Rectangle(BaseGame.Graphics.PreferredBackBufferWidth / 2 - 100, BaseGame.Graphics.PreferredBackBufferHeight / 2 - 20 + 75, 200, 40));
            buttonExitGame.Text = "exit";
            buttonExitGame.Color = Color.White;
            buttonExitGame.OnClick += new EventHandler(((Game1)BaseGame).Exit);
            gui.Add(buttonExitGame);

            BaseGame.IsMouseVisible = true;
            BaseGame.GameState = GameState.Menu;

            base.Load();
        }
    }
}
