﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using XGE;
using XGE.GameObjects;
using XGE.Engine;
using JigLibX;
using JigLibX.Collision;
using JigLibX.Physics;

namespace Amputator
{
    class Game1 : XGE.Engine.BaseGame
    {
        #region исползуемые поля

        private Levels.MainMenu mainMenu;
        private Levels.Menu gameMenu;
        private Levels.GameLevel1 level1;
        private List<Level> gameLevels;

        #endregion

        #region инициализация и загрузка

        public Game1()
            : base()
        {
            gameLevels = new List<Level>();
            mainMenu = new Amputator.Levels.MainMenu(this);
            gameMenu = new Amputator.Levels.Menu(this);
            level1 = new Amputator.Levels.GameLevel1(this);
        }
        protected override void Initialize()
        {
            RestoreRenderState();
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;
            //graphics.IsFullScreen = true;
            graphics.ApplyChanges();
            physicSystem.SolverType = PhysicsSystem.Solver.Fast;
            base.Initialize();
            physicSystem.NumCollisionIterations = 1;
            physicSystem.NumContactIterations = 1;
            
            mainMenu.Load();

        }

        #endregion

        #region обновление и отрисовка

        protected override void Update(GameTime gameTime)
        {
            HandleInput();
            switch (GameState)
            {
                case GameState.Paused:
                    {
                        Camera.State = XGE.Graphics.Camera.CameraState.Static;
                        break;
                    }
                case GameState.Menu:
                    {
                        Camera.State = XGE.Graphics.Camera.CameraState.Static;
                        break;
                    }
                case GameState.Playing:
                    {
                        Camera.State = XGE.Graphics.Camera.CameraState.Moving;

                        float timeStep = (float)gameTime.ElapsedGameTime.Ticks / TimeSpan.TicksPerSecond;
                        if (timeStep < 1.0f / 60.0f) physicSystem.Integrate(timeStep);
                        else physicSystem.Integrate(1.0f / 60.0f);

                        break;
                    }
            }
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            graphics.GraphicsDevice.Clear(Color.Black);
            base.Draw(gameTime);
        }

        #endregion

        #region ввод

        private void HandleInput()
        {
            KeyboardState keyState = Keyboard.GetState();
            MouseState mouseState = Mouse.GetState();

            if (keyState.IsKeyDown(Keys.Escape) && GameState != GameState.Menu)
            {
                if (!gameMenu.IsLoaded)
                    gameMenu.Load();
                else
                    gameMenu.Unload();
            }
        }
        public void Exit(object sender, EventArgs e)
        {
            base.Exit();
        }
        public void NextLevel(object sender, EventArgs e)
        {
            currentLevel.Unload();
            level1.Load();
        }

        #endregion
    }
}
