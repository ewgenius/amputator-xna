﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XGE.Engine;
using Microsoft.Xna.Framework;
using JigLibX.Collision;
using JigLibX.Physics;
using JigLibX.Geometry;
using JigLibX.Math;
using Microsoft.Xna.Framework.Graphics;


namespace XGE.GameObjects
{
    public class BoxObject : GameObject
    {

        public BoxObject(BaseGame game, Model model, Effect Effect, Vector3 sideLengths, Matrix orientation, Vector3 position)
            : base(game, model, Effect)
        {
            body = new Body();
            collision = new CollisionSkin(body);

            collision.AddPrimitive(new JigLibX.Geometry.Box(-0.5f * sideLengths, orientation, sideLengths), new MaterialProperties(0.8f, 0.8f, 0.7f));
            body.CollisionSkin = this.collision;
            Vector3 com = SetMass(1.0f);
            body.MoveTo(position, Matrix.Identity);
            collision.ApplyLocalTransform(new Transform(-com, Matrix.Identity));
            body.EnableBody();
            this.scale = sideLengths;
        }
        public void MoveTo(Vector3 pos,Matrix orentation)
        {
            body.MoveTo(pos,orentation);
        }
        public override void ApplyEffects(BasicEffect effect)
        {
            effect.DiffuseColor = color;
        }
    }
}
