﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using JigLibX.Collision;
using JigLibX.Physics;
using Microsoft.Xna.Framework.Graphics;
using XGE;
using XGE.Engine;

namespace XGE.GameObjects
{
    public class PlaneObject : GameObject
    {

        public PlaneObject(BaseGame game, Model model, Effect Effect, float d)
            : base(game, model, Effect)
        {
            body = new Body();
            collision = new CollisionSkin(null);
            collision.AddPrimitive(new JigLibX.Geometry.Plane(Vector3.Up, d), new MaterialProperties(0.2f, 0.7f, 0.6f));
            PhysicsSystem.CurrentPhysicsSystem.CollisionSystem.AddCollisionSkin(collision);
        }

        public override void ApplyEffects(BasicEffect effect)
        {
            //
        }
    }
}