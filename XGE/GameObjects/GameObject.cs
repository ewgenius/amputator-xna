﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using JigLibX.Physics;
using JigLibX.Collision;
using JigLibX.Geometry;
using XGE;
using XGE.Engine;
using XGE.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace XGE.GameObjects
{

    /// <summary>
    /// Helps to combine the physics with the graphics.
    /// </summary>
    public abstract class GameObject : BaseDrawableGameComponent
    {
        protected Body body;
        protected CollisionSkin collision;
        protected Model model;
        protected Vector3 color;
        protected Vector3 scale = new Vector3(1f, 1f, 1f);
        protected Effect effect;
        public AudioEmitter audioEmitter;
        public Body PhysicsBody { get { return body; } }
        public CollisionSkin PhysicsSkin { get { return collision; } }
        protected static Random random = new Random();

        public GameObject(BaseGame game, Model model, Effect Effect)
            : base(game)
        {
            this.model = model;
            Random rrr = new Random();

            color = new Vector3(rrr.Next(0, 255), rrr.Next(0, 255), rrr.Next(0, 255));

            audioEmitter = new AudioEmitter();               

            color /= 255.0f;
            effect = Effect;
        }

        public GameObject(BaseGame game)
            : base(game)
        {
            this.model = null;
            color = new Vector3(random.Next(255), random.Next(255), random.Next(255));
            color /= 255.0f;
        }

        protected Vector3 SetMass(float mass)
        {
            PrimitiveProperties primitiveProperties =
                new PrimitiveProperties(PrimitiveProperties.MassDistributionEnum.Solid, PrimitiveProperties.MassTypeEnum.Density, mass);

            float junk; Vector3 com; Matrix it, itCoM;

            collision.GetMassProperties(primitiveProperties, out junk, out com, out it, out itCoM);
            body.BodyInertia = itCoM;
            body.Mass = junk;

            return com;
        }
        Matrix[] boneTransforms = null;
        int boneCount = 0;

        public abstract void ApplyEffects(BasicEffect effect);
        public override void Update(GameTime gameTime)
        {
            audioEmitter.Position = body.Position;
            audioEmitter.Up = Vector3.Up;
            audioEmitter.Forward = Vector3.UnitX;

            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            if (model != null)
            {
                if (boneTransforms == null || boneCount != model.Bones.Count)
                {
                    boneTransforms = new Matrix[model.Bones.Count];
                    boneCount = model.Bones.Count;
                }

                model.CopyAbsoluteBoneTransformsTo(boneTransforms);

                Camera camera = BaseGame.Camera;
                foreach (ModelMesh mesh in model.Meshes)
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {

                        if (body.CollisionSkin != null)
                            effect.World = boneTransforms[mesh.ParentBone.Index] * Matrix.CreateScale(scale) * body.CollisionSkin.GetPrimitiveLocal(0).Transform.Orientation * body.Orientation * Matrix.CreateTranslation(body.Position);
                        else
                            effect.World = boneTransforms[mesh.ParentBone.Index] * Matrix.CreateScale(scale) * body.Orientation * Matrix.CreateTranslation(body.Position);
                        effect.AmbientLightColor = color;
                        effect.GraphicsDevice.RenderState.DepthBufferEnable = true;
                        effect.View = camera.View;
                        effect.Projection = camera.Projection;

                        ApplyEffects(effect);

                        effect.EnableDefaultLighting();
                        effect.PreferPerPixelLighting = true;
                    }
                    mesh.Draw();
                }
            }
            base.Draw(gameTime);
        }
    }
}