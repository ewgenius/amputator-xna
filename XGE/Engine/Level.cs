﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JigLibX;
using JigLibX.Collision;
using JigLibX.Physics;

namespace XGE.Engine
{
    public class Level : BaseGameComponent
    {
        protected List<Gui> gui;
        protected List<GameObjects.GameObject> objects;
        protected Boolean loaded;

        public Level(BaseGame game)
            : base(game)
        {
            gui = new List<Gui>();
            objects = new List<XGE.GameObjects.GameObject>();
            game.Components.Add(this);
        }

        public Boolean IsLoaded
        {
            get
            {
                return loaded;
            }
        }
        public virtual void Load()
        {
            foreach (GameObjects.GameObject obj in objects)
            {
                baseGame.Components.Add(obj);
            }
            foreach (Gui g in gui)
            {
                baseGame.Components.Add(g);
            }
            BaseGame.CurrentLevel = this;
            loaded = true;
        }
        public virtual void Unload()
        {
            foreach (GameObjects.GameObject obj in objects)
            {
                baseGame.Components.Remove(obj);
                obj.Dispose();
            }
            foreach (Gui g in gui)
            {
                baseGame.Components.Remove(g);
                g.Dispose();
            }
            objects.Clear();
            gui.Clear();
            loaded = false;
        }
    }
}
