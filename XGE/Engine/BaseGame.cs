﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using XGE.GameObjects;
using XGE.Engine;
using XGE.Graphics;
using JigLibX;
using JigLibX.Collision;
using JigLibX.Physics;
using JigLibX.Geometry;
using JigLibX.Math;
using JigLibX.Utils;

namespace XGE.Engine
{
    public class BaseGame : Game
    {
        protected Matrix view;
        protected Matrix world;
        protected Matrix projection;
        protected SpriteBatch sprite;
        protected BasicEffect effectBasic;
        protected Effect effect;
        protected Camera camera;
        protected GraphicsDeviceManager graphics;
        protected PhysicsSystem physicSystem;
        protected SpriteFont font;
        protected VideoPlayer vPlayer;
        protected GameState gamestate;
        protected ShaderState shaderstate;
        protected double FPS = 0;
        protected Int64 frames = 0;
        protected double timeElapsed = 0;
        protected Boolean callFps = true;
        protected Level currentLevel;
        protected SoundEngine sound;
        protected AudioListener audioListener;

        public BaseGame()
        {
            graphics = new GraphicsDeviceManager(this);

            Content.RootDirectory = "Content";
            
            camera = new Camera(this);
            camera.Position = new Vector3(-10, -10, -10);
            camera.Target = new Vector3(1, 1, 1);
            vPlayer = new VideoPlayer();

            graphics.SynchronizeWithVerticalRetrace = true;
            this.IsFixedTimeStep = true;
            physicSystem = new PhysicsSystem();
            graphics.SynchronizeWithVerticalRetrace = false;
            this.IsFixedTimeStep = false;
            physicSystem.CollisionSystem = new CollisionSystemSAP();

            physicSystem.EnableFreezing = true;
            physicSystem.SolverType = PhysicsSystem.Solver.Normal;
            physicSystem.CollisionSystem.UseSweepTests = true;

            physicSystem.NumCollisionIterations = 8;
            physicSystem.NumContactIterations = 8;
            physicSystem.NumPenetrationRelaxtionTimesteps = 15;

            
        }
        public SoundEngine Sound
        {
            get
            {
                return sound;
            }
            set
            {
                sound = (SoundEngine)value;
            }
        }
        public Matrix View
        {
            get
            {
                return view;
            }
            set
            {
                view = (Matrix)value;
            }
        }
        public Matrix World
        {
            get
            {
                return world;
            }
            set
            {
            }
        }
        public GraphicsDeviceManager Graphics
        {
            get
            {
                return graphics;
            }
        }
        public Matrix Projection
        {
            get
            {
                return projection;
            }
            set
            {
                projection = (Matrix)value;
            }
        }
        public SpriteBatch Sprite
        {
            get
            {
                return sprite;
            }
        }
        public BasicEffect EffectBasic
        {
            get
            {
                return effectBasic;
            }
            set
            {
                effectBasic = (BasicEffect)value;
            }
        }
        public Camera Camera
        {
            get
            {
                return camera;
            }
        }
        public VideoPlayer Player
        {
            get
            {
                return vPlayer;
            }
        }
        public GameState GameState
        {
            get
            {
                return gamestate;
            }
            set
            {
                gamestate = (GameState)value;
            }
        }
        public ShaderState ShaderState
        {
            get
            {
                return shaderstate;
            }
            set
            {
                shaderstate = (ShaderState)value;
            }
        }
        public Level CurrentLevel
        {
            get
            {
                return currentLevel;
            }
            set
            {
                currentLevel = (Level)value;
            }
        }

        protected void ResetScene()
        {

            List<BaseDrawableGameComponent> toRemove = new List<BaseDrawableGameComponent>();
            foreach (GameComponent component in Components)
            {
                if (component is BaseDrawableGameComponent)
                {
                    toRemove.Add(component as BaseDrawableGameComponent);
                }
            }
            foreach (BaseDrawableGameComponent obj in toRemove)
            {
                if (obj is GameObject && (obj as GameObject).PhysicsBody != null)
                    (obj as GameObject).PhysicsBody.DisableBody();
                Components.Remove(obj);
            }
            int count = physicSystem.Controllers.Count;
            for (int i = 0; i < count; i++)
                physicSystem.Controllers[0].DisableController();
            count = physicSystem.Constraints.Count;
            for (int i = 0; i < count; i++)
                physicSystem.RemoveConstraint(physicSystem.Constraints[0]);
            //Mouse.SetPosition(Camera.widthOver2, Camera.heightOver2);
        }
        protected void RestoreRenderState()
        {
            this.GraphicsDevice.RenderState.DepthBufferEnable = true;
            this.graphics.GraphicsDevice.RenderState.DepthBufferEnable = true;
            this.graphics.GraphicsDevice.RenderState.AlphaTestEnable = true;
            this.graphics.GraphicsDevice.SamplerStates[0].AddressU = TextureAddressMode.Wrap;
            this.graphics.GraphicsDevice.SamplerStates[0].AddressV = TextureAddressMode.Wrap;
            this.graphics.GraphicsDevice.RenderState.AlphaBlendEnable = true;
            this.graphics.ApplyChanges();
        }
        protected override void Update(GameTime gameTime)
        {
            
            //audioListener.Position = Camera.Position;
            //audioListener.Up = Vector3.UnitY;
            //audioListener.Forward = Camera.Target;
              

            base.Update(gameTime);
            
        }
        protected override void Initialize()
        {
            effectBasic = new BasicEffect(graphics.GraphicsDevice, null);
            sprite = new SpriteBatch(graphics.GraphicsDevice);
            font = Content.Load<SpriteFont>(@"SystemFont");

            View = Matrix.CreateLookAt(new Vector3(10, 10, 10), new Vector3(-1, -1, -1), new Vector3(0, 0, 1));
            Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, 800 / 600, 1, 10000);
            World = Matrix.CreateTranslation(new Vector3(0, 0, 0));

            Components.Add(camera);

            RestoreRenderState();

            sound = new SoundEngine(this, "sound.xgs");

            sound.PlayMusic("Music");

            base.Initialize();
        }
        protected override void UnloadContent()
        {
            Content.Unload();
            base.UnloadContent();
        }
        protected void CallFps(double time)
        {
            frames++;
            timeElapsed += time;
            if (timeElapsed >= 100.0f)
            {
                FPS = Math.Round((double)1000 * frames / (double)timeElapsed, 1);
                timeElapsed = 0;
                frames = 0;
            }
            sprite.DrawString(font, string.Format("FPS={0}", FPS), new Vector2(5, 5), Color.White);
        }
        protected override void Draw(GameTime gameTime)
        {
            Sprite.Begin();
            base.Draw(gameTime);
            if (callFps) CallFps(gameTime.ElapsedRealTime.TotalMilliseconds);
            sprite.DrawString(font, string.Format("Components={0}", Components.Count), new Vector2(5, 20), Color.White);
            Sprite.End();

        }
    }
}
