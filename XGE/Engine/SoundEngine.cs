﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace XGE.Engine
{
    public class SoundEngine : BaseGameComponent
    {
        protected AudioEngine audioEngine;
        protected WaveBank waveBank;
        protected SoundBank soundBank;
        protected AudioEmitter emitter;
        protected AudioListener listener;
        protected Cue musicCue;

        #region Конструктор
        public SoundEngine(BaseGame game, String soundBankName)
            : base(game)
        {
            audioEngine = new AudioEngine(@"Content\Audio\"+soundBankName);
            soundBank = new SoundBank(audioEngine, @"Content\Audio\Sound Bank.xsb");
            waveBank = new WaveBank(audioEngine, @"Content\Audio\Wave Bank.xwb");
            emitter = new AudioEmitter();
            listener = new AudioListener();
        }
        #endregion
        #region Музыка
        public void PlayMusic(String musicName)
        {
            musicCue = soundBank.GetCue(musicName);
            musicCue.Play();
        }
        public void StopMusic()
        {
            musicCue.Stop(AudioStopOptions.AsAuthored);
        }
        #endregion

        //private методы 

        #region Звук от объекта
        private void PlaySound(String soundName, Matrix listenerOrientation, Matrix emitterOrientation)
        {
            Cue cue = soundBank.GetCue(soundName);
            
            listener.Position = listenerOrientation.Translation;
            listener.Forward = listenerOrientation.Forward;
            listener.Up = listenerOrientation.Up;

            emitter.Position = emitterOrientation.Translation;
            emitter.Forward = emitterOrientation.Forward;
            emitter.Up = emitterOrientation.Up;

            cue.Apply3D(listener, emitter);

            soundBank.PlayCue(soundName);
        }
        

        private void PlaySound(String soundName)
        {
            soundBank.PlayCue(soundName);
        }
        #endregion

        //public методы
        public void PlaySoundDudeDeath()
        {
            //PlaySound("", listenerOrientation, emitterOrientation);
        }
        public void PlaySoundDudeAttack()
        {
            //PlaySound("", , );
        }
        public void PlaySoundPlayerDeath()
        {
            //PlaySound("", , );
        }
        public void PlaySoundPlayerAttack()
        {
            //PlaySound("", , );
            //audioEngine.GetCategory();
        }
        
    }

}
