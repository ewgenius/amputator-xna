﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace XGE.Engine
{
    public abstract class Gui : BaseDrawableGameComponent
    {
        protected Rectangle rectangle;
        protected Texture2D texture;
        protected Color color;
        protected String name;

        public Gui(BaseGame game)
            : base(game)
        {
        }

        public Texture2D Texture
        {
            get
            {
                return texture;
            }
        }
        public String Name
        {
            get
            {
                return name;
            }
            set
            {
                name = (String)value;
            }
        }
        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = (Color)value;
            }
        }
    }
}
