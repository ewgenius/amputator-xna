﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XGE
{
    public enum GameState
    {
        Intro = 0,
        Menu = 1,
        Paused = 2,
        Playing = 3,
    }
}
