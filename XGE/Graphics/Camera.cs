﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using XGE.Engine;

namespace XGE.Graphics
{
    public class Camera : BaseGameComponent
    {
        public enum CameraState
        {
            Static = 0,
            Moving = 2,
        }
        private CameraState cameraState;
        protected Matrix view;
        protected Matrix projection;
        protected Vector3 position;
        protected Vector3 target;
        protected Vector3 angles;
        protected float aspectRatio;
        protected float fieldOfView;
        protected float nearPlaneDistance;
        protected float farPlaneDistance;
        protected MouseState mouseState;
        public int widthOver2;
        public int heightOver2;
        protected float speed = 0.5f;

        public Camera(BaseGame game)
            : base(game)
        {
            mouseState = new MouseState();
            position = new Vector3(0, 100, 100);
            target = -position;
            angles = new Vector3(0, 0, 0);
            aspectRatio = (float)BaseGame.Graphics.PreferredBackBufferWidth / (float)BaseGame.Graphics.PreferredBackBufferHeight;
            fieldOfView = MathHelper.PiOver4;
            nearPlaneDistance = 1.0f;
            farPlaneDistance = 10000.0f;
            widthOver2 = BaseGame.Graphics.PreferredBackBufferWidth / 2;
            heightOver2 = BaseGame.Graphics.PreferredBackBufferHeight / 2;
            Mouse.SetPosition(widthOver2, heightOver2);
        }

        public Vector3 Position
        {
            get
            {
                return position;
            }
            set
            {
            }
        }
        public Vector3 Target
        {
            get
            {
                Matrix cameraRotation = Matrix.CreateRotationX(angles.X) * Matrix.CreateRotationY(angles.Y);
                target = position + Vector3.Transform(Vector3.Forward, cameraRotation);
                return target;
            }
            set
            {
            }
        }
        public Matrix View
        {
            get
            {
                return view;
            }
            set
            {
            }
        }
        public Matrix Projection
        {
            get
            {
                return projection;
            }
            set
            {
            }
        }
        public float NearPlaneDistance
        {
            get
            {
                return nearPlaneDistance;
            }
        }
        public float FarPlaneDistance
        {
            get
            {
                return farPlaneDistance;
            }
        }
        public CameraState State
        {
            get
            {
                return cameraState;
            }
            set
            {
                cameraState = (CameraState)value;
            }
        }

        public override void Update(GameTime gameTime)
        {
            widthOver2 = BaseGame.Graphics.PreferredBackBufferWidth / 2;
            heightOver2 = BaseGame.Graphics.PreferredBackBufferHeight / 2;
            if (cameraState == CameraState.Moving)
            {
                base.Update(gameTime);
                float time = (float)gameTime.ElapsedGameTime.Ticks / (float)200000;

                Vector3 moveVector = new Vector3();

                KeyboardState keys = Keyboard.GetState();
                if (keys.IsKeyDown(Keys.D))
                    moveVector.X += speed * time;
                if (keys.IsKeyDown(Keys.A))
                    moveVector.X -= speed * time;
                if (keys.IsKeyDown(Keys.S))
                    moveVector.Z += speed * time;
                if (keys.IsKeyDown(Keys.W))
                    moveVector.Z -= speed * time;

                Matrix cameraRotation = Matrix.CreateRotationX(angles.X) * Matrix.CreateRotationY(angles.Y);
                position += Vector3.Transform(moveVector, cameraRotation);

                MouseState currentMouseState = Mouse.GetState();


                if (currentMouseState.X != widthOver2)
                    angles.Y -= speed * time / 80.0f * (currentMouseState.X - widthOver2);
                if (currentMouseState.Y != heightOver2)
                    angles.X -= speed * time / 80.0f * (currentMouseState.Y - heightOver2);

                if (angles.X > 1.4) angles.X = 1.4f;
                if (angles.X < -1.4) angles.X = -1.4f;
                if (angles.Y > Math.PI) angles.Y -= 2 * (float)Math.PI;
                if (angles.Y < -Math.PI) angles.Y += 2 * (float)Math.PI;

                Mouse.SetPosition(widthOver2, heightOver2);

                Matrix camerarotation = Matrix.CreateRotationX(angles.X) * Matrix.CreateRotationY(angles.Y);
                Vector3 targetPos = position + Vector3.Transform(Vector3.Forward, camerarotation);
                Vector3 upVector = Vector3.Transform(Vector3.Up, camerarotation);
                view = Matrix.CreateLookAt(position, targetPos, upVector);
                projection = Matrix.CreatePerspectiveFieldOfView(fieldOfView, aspectRatio, nearPlaneDistance, farPlaneDistance);

                baseGame.View = view;
                baseGame.Projection = projection;
            }
            else
            {
            }
        }
    }
}
